from mpl_toolkits.basemap import Basemap
from skyfield.api import Loader, Topos

import datetime
import matplotlib.pyplot as plt
import numpy as np

load = Loader('~/skyfield-data')

satellites = load.tle('http://celestrak.com/NORAD/elements/tle-new.txt')
satellite = satellites['2019-068B']

ts = load.timescale()

plt.ion()

ocean_map = (plt.get_cmap('ocean'))(210)
cmap = plt.get_cmap('gist_earth')

while True:
  plt.cla()

  now = datetime.datetime.utcnow()
  print(now)
  t = ts.utc(now.year, now.month, now.day, now.hour, now.minute, now.second)
  geocentric = satellite.at(t)
  subpoint = geocentric.subpoint()

  map = Basemap(projection='ortho', lon_0=subpoint.longitude.degrees, lat_0=subpoint.latitude.degrees)
  map.drawcoastlines()
  map.drawcountries()
  map.fillcontinents(color=cmap(200), lake_color=ocean_map)
  map.drawmapboundary(fill_color=ocean_map)
  map.drawmeridians(np.arange(0, 360, 30))
  map.drawparallels(np.arange(-90, 90, 30))
  map.plot(x=subpoint.longitude.degrees, y=subpoint.latitude.degrees,
           marker='*', linestyle='dotted', color='r', latlon=True)
  plt.draw()
  plt.pause(10)
